package com.lanchong.config;

import liquibase.integration.spring.SpringLiquibase;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

//@ConditionalOnProperty(value = "spring.profiles.active", havingValue = "dev")
@SpringBootConfiguration
public class LiquibaseConfig {

    public static final String CHANGE_LOG_PATH = "classpath:/liquibase/db.changelog-master.xml";

    @Bean
    public SpringLiquibase liquibase(DataSource dataSource) {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog(CHANGE_LOG_PATH);
        liquibase.setDataSource(dataSource);
        liquibase.setShouldRun(true);
        return liquibase;
    }

}